import React from 'react';
import { render, cleanup } from '@testing-library/react';

import Message from './Message';

describe('<Message />', () => {
  afterEach(cleanup);

  it('renders without crashing', () => {
    render(<Message message="test" />);
  });

  it('renders with given message', () => {
    const { queryByText } = render(<Message message="test" />);

    expect(queryByText('test')).not.toBeNull();
  });
});
